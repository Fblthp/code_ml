#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/JSONNodeDumper.h"
#include <iostream>

using namespace clang;
clang::LangOptions lopt;

int cnt = 0;

// std::string decl2str(clang::Decl *d, SourceManager* sm) {
//     clang::SourceLocation b(d->getLocStart()), _e(d->getLocEnd());
//     clang::SourceLocation e(clang::Lexer::getLocForEndOfToken(_e, 0, *sm, lopt));
//     return std::string(sm->getCharacterData(b),
//         sm->getCharacterData(e)-sm->getCharacterData(b));
// }

std::string decl2str(clang::Decl *d, SourceManager &sm) {
    // (T, U) => "T,,"
    std::string text = Lexer::getSourceText(CharSourceRange::getTokenRange(d->getSourceRange()), sm, LangOptions(), 0);
    if (text.size() > 0 && (text.at(text.size()-1) == ',')) //the text can be ""
        return Lexer::getSourceText(CharSourceRange::getCharRange(d->getSourceRange()), sm, LangOptions(), 0);
    return text;
}

std::string stmt2str(clang::Stmt *d, SourceManager &sm) {
    // (T, U) => "T,,"
    std::string text = Lexer::getSourceText(CharSourceRange::getTokenRange(d->getSourceRange()), sm, LangOptions(), 0);
    if (text.size() > 0 && (text.at(text.size()-1) == ',')) //the text can be ""
        return Lexer::getSourceText(CharSourceRange::getCharRange(d->getSourceRange()), sm, LangOptions(), 0);
    return text;
}

class MyClass : public RecursiveASTVisitor<MyClass> {
public:
    explicit MyClass(ASTContext *Context, SourceManager& SM) : Context(Context), SM(SM) {}
    void PrintPos() {
      // FullSourceLoc FullLocation = Context->getFullLoc(Declaration->getBeginLoc());
    }
    bool TraverseDecl(Decl *D) {
        for (int i = 0; i < cnt; ++i) {
          llvm::outs() << " ";
        }
        cnt += 4;
        llvm::outs() << "decl " << decl2str(D, SM) << " " << D->getDeclKindName() << "\n";
        RecursiveASTVisitor<MyClass>::TraverseDecl(D); // Forward to base class

        cnt -= 4;
        return true;
    }
    bool TraverseStmt(Stmt *x) {
        // your logic here
        for (int i = 0; i < cnt; ++i) {
          llvm::outs() << " ";
        }
        cnt += 4;
        // x->dump();
        llvm::outs() << "stmt "  << stmt2str(x, SM) << " " << x->getStmtClassName() << "\n";
        RecursiveASTVisitor<MyClass>::TraverseStmt(x);
        cnt -= 4;
        return true;
    }
    bool TraverseType(QualType x) {
        // your logic here
        RecursiveASTVisitor<MyClass>::TraverseType(x);
        return true;
    }
  ASTContext *Context;
  SourceManager& SM;

};

// class FindNamedClassVisitor
//   : public RecursiveASTVisitor<FindNamedClassVisitor> {
// public:
//   explicit FindNamedClassVisitor(ASTContext *Context)
//     : Context(Context) {}

//   bool VisitCXXRecordDecl(CXXRecordDecl *Declaration) {
//     for (int i = 0; i < cnt; ++i) {
//       cout << 
//     }
//     if (Declaration->getQualifiedNameAsString() == "n::m::C") {
//       FullSourceLoc FullLocation = Context->getFullLoc(Declaration->getBeginLoc());
//       if (FullLocation.isValid())
//         llvm::outs() << "Found declaration at "
//                      << FullLocation.getSpellingLineNumber() << ":"
//                      << FullLocation.getSpellingColumnNumber() << "\n";
//     }
//     return true;
//   }

// private:
//   ASTContext *Context;
// };

 // clang::JSONNodeDumper::JSONNodeDumper(llvm::raw_ostream&, const clang::SourceManager&, clang::ASTContext&, 
 //  const clang::PrintingPolicy&, const clang::comments::CommandTraits*)

// class FindNamedClassConsumer : public clang::ASTConsumer {
// public:
//   explicit FindNamedClassConsumer(ASTContext* Context) //, SourceManager& SM,  clang::PrintingPolicy& Policy)
//     : Visitor(Context) {}

//   virtual void HandleTranslationUnit(clang::ASTContext &Context) {
//     Visitor.Visit(Context.getTranslationUnitDecl());
//   }
// private:
//   JSONNodeDumper Visitor;
// };

// class FindNamedClassAction : public clang::ASTFrontendAction {
// public:
//   virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
//     clang::CompilerInstance &Compiler, llvm::StringRef InFile) {

//     // clang::LangOptions LangOpts;
//     // LangOpts.CPlusPlus = true;
//     // clang::PrintingPolicy Policy(LangOpts);
//     return std::unique_ptr<clang::ASTConsumer>(
//         new FindNamedClassConsumer(Compiler.getASTContext())); //, Policy); //Compiler.getSourceManager(), 
//   }
// };

class FindNamedClassConsumer : public clang::ASTConsumer {
public:
  explicit FindNamedClassConsumer(ASTContext *Context, SourceManager& SM)
    : Visitor(Context, SM) {}

  virtual void HandleTranslationUnit(clang::ASTContext &Context) {
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  }
private:
  MyClass Visitor;
};

class FindNamedClassAction : public clang::ASTFrontendAction {
public:
  virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
    clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
    return std::unique_ptr<clang::ASTConsumer>(
        new FindNamedClassConsumer(&Compiler.getASTContext(), Compiler.getSourceManager()));
  }
};


int main(int argc, char **argv) {
  if (argc > 1) {
    clang::tooling::runToolOnCode(new FindNamedClassAction, argv[1]);
  }
}