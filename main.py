import pandas as pd
import numpy as np
import lex
import torch
import torch.nn as nn
import torch.nn.functional as F
from models import *

df = pd.read_csv('data/df_filtered_tags.csv')
tag_list = ['data structures', 'dp', 'greedy', 'geometry']

print('building dicts..')
res = df.groupby('problem_id')
id_to_cnt = {pr_id: cnt for pr_id, cnt in res.size().sort_values().iteritems()}
id_to_tag = {row[1]['problem_id'] : row[1]['tag'] for row in df.iterrows()}

tags = set(df['tag'])
tag_to_problem_list = {t:{'problem_list' : [], 'cnt' : 0} for t in tags}
for pr, cnt in id_to_cnt.items():
    tag = id_to_tag[pr]
    tag_to_problem_list[tag]['problem_list'].append(pr)
    tag_to_problem_list[tag]['cnt'] += cnt

tags_df = pd.DataFrame.from_dict(tag_to_problem_list, orient='index').reset_index()
print('done..')

print('splitting data..')
def get_train_test():
    train_ids, test_ids = [], []
    for t in tag_list:
        pr_list = tag_to_problem_list[t]['problem_list']
        total = len(pr_list)
        if t == 'greedy':
            total -= 13
        bound = int(total * 0.5)
        train_ids.append(pr_list[bound:total])
        test_ids.append(pr_list[:bound])
    train_ids = [a for b in train_ids for a in b]
    test_ids = [a for b in test_ids for a in b]
    
    return train_ids, test_ids

train_ids, test_ids = get_train_test()

train_df = df[df['problem_id'].isin(train_ids)]
test_df = df[df['problem_id'].isin(test_ids)]

print('done, train_df size : {0}, test_df size : {1}'.format(len(train_df), len(test_df)))

print('lexing train data..')
lexer = lex.create_lexer()
eos = '_EOS_'
bos = '_BOS_'

comment_types = ['LINE_COMMENT', 'AREA_COMMENT', 'AREA_COMMENT_CONTINUE']
def get_token_list(data):
    lexer.input(data)
    return np.array([[t.value, t.type] for t in lexer if t.type not in comment_types])
    return pd.Series([tokens[:,0], tokens[:,1]])

train_df['tokens'] = train_df['source'].apply(get_token_list)

all_tokens = set()
for r in train_df['tokens']:
    for t in r:
        all_tokens.add(t[0]) # ignore token type
print('done, {0} tokens total'.format(len(all_tokens)))

all_tokens.add(eos)
all_tokens.add(bos)

token_to_id = {t:i for i,t in enumerate(all_tokens)}

def to_matrix(data):
    # ignore token type for now
    max_len = max(map(lambda x : len(x[:,0]), data))
    matrix = np.zeros((len(data), max_len), dtype='int32') + token_to_id[eos]
    for i, seq in enumerate(data):
        tokens = seq[:, 0]
        row_ix = list(map(lambda x : token_to_id[x], tokens))[:max_len]
        matrix[i, :len(row_ix)] = row_ix
    return matrix

print('matrix shape: {0}'.format(to_matrix(train_df['tokens'][:2]).shape))

def get_tag_id(log_probas):
    return tag_list[log_probas.argmax()]

cuda = torch.device('cuda')

print('init model')
# model_ = nn.DataParallel(BasicLSTMModel(len(token_to_id), token_to_id[eos], emb_size=128, hid_size=256, out_size=len(tag_list)).to(device=cuda))
model_ = BasicLSTMModel(len(token_to_id), token_to_id[eos], emb_size=128, hid_size=256, out_size=len(tag_list)).to(device=cuda)

inp = torch.tensor(to_matrix(train_df['tokens'][:5]), dtype=torch.int64).to(device=cuda)
print('check correctness. Getting tag : {0}'.format(get_tag_id(model_.forward(inp)[0])))

tag_to_id = {i:t for t, i in enumerate(tag_list)}

def compute_loss_on_batch(input_sequence, reference_answers):
    input_sequence = torch.tensor(to_matrix(input_sequence), dtype=torch.int64).to(device=cuda)

    logprobs_seq =  model_.forward(input_sequence)
    loss = nn.CrossEntropyLoss()
    ans = torch.tensor([tag_to_id[ix] for i, ix in enumerate(reference_answers)], dtype=torch.int64).to(device=cuda)
    return loss(logprobs_seq, ans)

print('checking loss computation works: ', compute_loss_on_batch(train_df['tokens'][:32], train_df['tag'][:32]))

train_df = train_df.reset_index()

print('start training')
from tqdm import tqdm, trange  # or use tqdm_notebook,tnrange
import matplotlib.pyplot as plt

REPORT_FREQ = 100
loss_history = []
editdist_history = []
entropy_history = []
opt = torch.optim.Adam(model_.parameters())

def sample_batch(df, batch_size):
    idxs = np.random.choice(len(df), size=batch_size)
    return df['tokens'][idxs], df['tag'][idxs]

for i in trange(6000):
    loss = compute_loss_on_batch(*sample_batch(train_df, 16))

    loss.backward()
    opt.step()
    opt.zero_grad()

    loss_history.append(loss.item())

    if (i+1) % REPORT_FREQ == 0:
        # print("llh=%.3f" % (np.mean(loss_history[-10:])))
        plt.plot(loss_history)
        plt.grid()
        plt.savefig('loss.png')

print("llh=%.3f" % (np.mean(loss_history[-10:])))
print('done training, saving model')
model_name = '16_tag_LSTM_model.pt'
torch.save(model_.state_dict(), model_name)
print('saved {0}'.format(model_name))

print('calculating test score')
test_df_ = test_df.reset_index()
print('tokenize')
test_df_['tokens'] = test_df_['source'].apply(get_token_list)

def test_row_to_matrix(seq):
    tokens = [t for t in seq[:, 0] if t in token_to_id]
    matrix = np.zeros((1, len(tokens)), dtype='int32') + token_to_id[eos]
    matrix[0, :len(tokens)] = list(map(lambda x : token_to_id[x], tokens))
    return matrix

print('convert to matrix')
test_df_['inps'] = test_df_['tokens'].apply(test_row_to_matrix)

def get_ans(inp):
    inp = torch.tensor(inp, dtype=torch.int64).to(device=cuda)
    probas = model_.forward(inp)
    return tag_list[probas.argmax()]

print('evaluate')
pred_y = test_df_['inps'].apply(get_ans)

print('Acc: {0}'.format(np.sum(pred_y == test_df_['tag']) / len(test_df_)))
