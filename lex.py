import numpy as np
import ply.lex as lex

tokens = (
   'LINE_COMMENT',
   'AREA_COMMENT',
   'AREA_COMMENT_CONTINUE',
   'NUMBER',
   'OPERATOR',
   'QUOTE',
   'CHAR',
   'CHAR_CONTINUE',
   'DIRECTIVE',
   'IDENTIFIER',
   'OPEN_PAREN',
   'CLOSE_PAREN',
   'OPEN_CURLY',
   'CLOSE_CURLY',
   'OPEN_SQUARE',
   'CLOSE_SQUARE'
)

def t_LINE_COMMENT(t):
    r'\/\/[^\n]*'
    return t

def t_AREA_COMMENT(t):
    r'\/\*([^*]|\*(?!\/))*\*\/'
    return t

def t_AREA_COMMENT_CONTINUE(t):
    r'\/\*([^*]|\*(?!\/))*\*'
    return t

def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_OPERATOR(t):
    #r'[-\*/\+><=]'
    r'([-<>~!%^&*\/+=?|.,:;]|->|<<|>>|\*\*|\|\||&&|--|\+\+|[-+*|&%\/=]=)'
    return t

def t_QUOTE(t):
    r'"([^"\n]|\\")*"?'
    return t

def t_CHAR(t):
    r'\'(\\?[^\'\n]|\\\')\''
    return t

def t_CHAR_CONTINUE(t):
    r'\'[^\']*'
    return t

def t_DIRECTIVE(t):
    r'\#(\S*)'
    return t

def t_IDENTIFIER(t):
    r'[_A-Za-z][0-9_A-Za-z]*'
    return t

t_OPEN_PAREN = r'\('
t_CLOSE_PAREN = r'\)'
t_OPEN_CURLY = r'{'
t_CLOSE_CURLY = r'}'
t_OPEN_SQUARE = r'\['
t_CLOSE_SQUARE = r'\]'

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    #print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

cpp_keywords = {
    "alignas",
    "alignof",
    "and",
    "and_eq",
    "asm",
    "atomic_cancel",
    "atomic_commit",
    "atomic_noexcept",
    "auto",
    "bitand",
    "bitor",
    "bool",
    "break",
    "case",
    "catch",
    "char",
    "char16_t",
    "char32_t",
    "class",
    "compl",
    "concept",
    "const",
    "constexpr",
    "const_cast",
    "continue",
    "co_await",
    "co_return",
    "co_yield",
    "decltype",
    "default",
    "delete",
    "do",
    "double",
    "dynamic_cast",
    "else",
    "enum",
    "explicit",
    "export",
    "extern",
    "false",
    "float",
    "for",
    "friend",
    "goto",
    "if",
    "import",
    "inline",
    "int",
    "long",
    "module",
    "mutable",
    "namespace",
    "new",
    "noexcept",
    "not",
    "not_eq",
    "nullptr",
    "operator",
    "or",
    "or_eq",
    "private",
    "protected",
    "public",
    "register",
    "reinterpret_cast",
    "requires",
    "return",
    "short",
    "signed",
    "sizeof",
    "static",
    "static_assert",
    "static_cast",
    "struct",
    "switch",
    "synchronized",
    "template",
    "this",
    "thread_local",
    "throw",
    "true",
    "try",
    "typedef",
    "typeid",
    "typename",
    "union",
    "unsigned",
    "using",
    "virtual",
    "void",
    "volatile",
    "wchar_t",
    "while",
    "xor",
    "xor_eq",
    "{",
    "}"
}

comment_types = [
    'LINE_COMMENT', 
    'AREA_COMMENT', 
    'AREA_COMMENT_CONTINUE',
]

def create_lexer():
    return lex.lex()

def get_token_list(data, ignore_comments=True):
    lexer.input(data)
    return np.array([[t.value, t.type] for t in lexer if not ignore_comments or t.type not in comment_types])

def get_space_separated_token_list(data):
    lexer.input(data)
    return ' '.join([str(t.value) for t in lexer])

lexer = create_lexer()
