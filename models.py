import torch
import torch.nn as nn
import torch.nn.functional as F

class BasicLSTMModel(nn.Module):
    def __init__(self, inp_voc_len, eos_ix, emb_size, hid_size, out_size):
        super(self.__class__, self).__init__()
        self.inp_voc_len = inp_voc_len
        self.eos_ix = eos_ix
        self.emb_inp = nn.Embedding(inp_voc_len, emb_size)

        self.enc0 = nn.LSTM(emb_size, hid_size, batch_first=True)
        self.outs = nn.Linear(hid_size, hid_size)
        self.logits = nn.Linear(hid_size, out_size)

    def forward(self, inp, eps=1e-30, **flags):
        batch_size = inp.shape[0]

        inp_emb = self.emb_inp(inp)
        # self.enc0.flatten_parameters()
        enc_seq, _ = self.enc0(inp_emb)

        end_index = infer_length(inp, self.eos_ix)
        end_index[end_index >= inp.shape[1]] = inp.shape[1] - 1
        enc_last = enc_seq[range(0, enc_seq.shape[0]), end_index.detach(), :]

        hid_state = self.outs(enc_last)

        logits_seq = self.logits(hid_state)

        return logits_seq

class BasicBiLSTMModel(BasicLSTMModel):
    def __init__(self, inp_voc_len, eos_ix, emb_size, hid_size, out_size):
        nn.Module.__init__(self)
        self.inp_voc_len = inp_voc_len
        self.eos_ix = eos_ix
        self.emb_inp = nn.Embedding(inp_voc_len, emb_size)

        self.enc0 = nn.LSTM(emb_size, hid_size, bidirectional=True, batch_first=True)
        self.outs = nn.Linear(2 * hid_size, hid_size)
        self.logits = nn.Linear(hid_size, out_size)


def infer_mask(seq, eos_ix, batch_first=True, include_eos=True, dtype=torch.float):
    """
    compute length given output indices and eos code
    :param seq: tf matrix [time,batch] if batch_first else [batch,time]
    :param eos_ix: integer index of end-of-sentence token
    :param include_eos: if True, the time-step where eos first occurs is has mask = 1
    :returns: lengths, int32 vector of shape [batch]
    """
    assert seq.dim() == 2
    is_eos = (seq == eos_ix).to(dtype=torch.float)
    if include_eos:
        if batch_first:
            is_eos = torch.cat((is_eos[:,:1]*0, is_eos[:, :-1]), dim=1)
        else:
            is_eos = torch.cat((is_eos[:1,:]*0, is_eos[:-1, :]), dim=0)
    count_eos = torch.cumsum(is_eos, dim=1 if batch_first else 0)
    mask = count_eos == 0
    return mask.to(dtype=dtype)

def infer_length(seq, eos_ix, batch_first=True, include_eos=True, dtype=torch.long):
    """
    compute mask given output indices and eos code
    :param seq: tf matrix [time,batch] if time_major else [batch,time]
    :param eos_ix: integer index of end-of-sentence token
    :param include_eos: if True, the time-step where eos first occurs is has mask = 1
    :returns: mask, float32 matrix with '0's and '1's of same shape as seq
    """
    mask = infer_mask(seq, eos_ix, batch_first, include_eos, dtype)
    return torch.sum(mask, dim=1 if batch_first else 0)
